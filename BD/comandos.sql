CREATE TABLE Usuario(
    id_usuario INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    email VARCHAR(50) NOT NULL UNIQUE,
    nombre VARCHAR(80) NOT NULL,
    apellido VARCHAR(80) NOT NULL,
    password VARCHAR(128) NOT NULL,
    id_rol INTEGER,
    FOREIGN KEY(id_rol) REFERENCES Rol(id_rol)
);

CREATE TABLE Rol(
    id_rol INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    tipo VARCHAR(50) NOT NULL UNIQUE
);

CREATE TABLE Topic(
    id_topic INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    descripcion VARCHAR(50) NOT NULL UNIQUE
);

CREATE TABLE Post(
    id_post INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    id_usuario INTEGER,
    id_topic INTEGER,
    fecha_creacion DATE,
    titulo VARCHAR(50) NOT NULL,
    contenido VARCHAR(1000) NOT NULL,
    FOREIGN KEY(id_usuario) REFERENCES Usuario(id_usuario),
    FOREIGN KEY(id_topic) REFERENCES Topic(id_topic)
);

CREATE TABLE comentario(
    id_comentario INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    id_usuario INTEGER,
    id_post INTEGER,
    fecha DATE,
    comentario VARCHAR(500) NOT NULL,
    FOREIGN KEY(id_usuario) REFERENCES Usuario(id_usuario),
    FOREIGN KEY(id_post) REFERENCES Post(id_post)
);
