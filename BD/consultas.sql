# Listar a todos los usuarios
select nombre, apellido from USUARIO;

# Listar todos los post del usuario 8
select titulo from POST where id_usuario = 8;

# Listar todos los comentarios del usuario 7
select comentario from COMENTARIO where id_usuario = 7;

# Mostrar la cantidad de posteos por usuario
select id_usuario, count(*) from POST group by id_usuario;
