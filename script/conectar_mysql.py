import pymysql
from datetime import datetime

# Abre conexion con la BD
db = pymysql.connect('localhost', 'root', 'eduardo123', 'Blog')
# prepara el cursor
cursor = db.cursor()


def consultar_todo(tabla):
    """ Recibe como parametro la tabla que se desea consultar   """
    sql = 'SELECT * FROM {}'.format(tabla)
    try:
        # ejecuta la Query usando el metodo execute()
        cursor.execute(sql)
        # procesa una unica linea usando el metodo fetchone()
        # procesa todos los registros de la tabla usando el metodo fetchall()
        data = cursor.fetchall()
        # print("Resultado de la consulta = {}".format(data))
        for i in data:
            print(i)
    except Exception as e:
        print(e, "***** ERROR en la consulta *****")
        db.rollback()


def insertar_post(id_usuario, id_topic, titulo, contenido):
    """ Recibe como parametro el id del usuario, id del topic, el titulo del post y el contenido del post"""
    sql = 'INSERT INTO POST(id_usuario, id_topic, fecha_creacion, titulo, contenido)   ' \
          'VALUES(%s, %s, %s, %s, %s)'
    try:
        cursor.execute(sql, (id_usuario, id_topic, datetime.now(), titulo, contenido))
        db.commit()
    except Exception as e:
        print(e, '***** ERROR al insertar el post *****')
        db.rollback()


def insertar_comentario(id_usuario, id_post, comentario):
    """ Recibe como parametro el id del usuario, id_post y el comentario para el post"""
    sql = 'INSERT INTO COMENTARIO(id_usuario, id_post, fecha, comentario)   ' \
          'VALUES(%s, %s, %s, %s)'
    try:
        cursor.execute(sql, (id_usuario, id_post, datetime.now(), comentario))
        db.commit()
    except Exception as e:
        print(e, '***** ERROR al insertar el comentario *****')
        db.rollback()


def insertar_usuario(email, nombre, apellido, password, id_rol):
    """ Recibe como parametro el email, nombre, apellido, password y id del rol que va a tener el usuario """
    sql = 'INSERT INTO USUARIO(email, nombre, apellido, password, id_rol)   ' \
          'VALUES(%s, %s, %s, %s, %s)'
    try:
        cursor.execute(sql, (email, nombre, apellido, password, id_rol))
        db.commit()
    except Exception as e:
        print(e, "***** ERROR al insertar un usuario *****")
        db.rollback()


def insertar_rol(tipo):
    """ Recibe como parametro el tipo de rol a dar de alta en el blog"""
    sql = 'INSERT INTO ROL(tipo)   ' \
          'VALUES(%s)'
    try:
        cursor.execute(sql, tipo)
        db.commit()
    except Exception as e:
        print(e, "***** ERROR al insertar un rol *****")
        db.rollback()


def insertar_topic(descripcion):
    """ recibe como parametro el nombre del topic """
    sql = 'INSERT INTO TOPIC(descripcion)   ' \
          'VALUES(%s)'
    try:
        cursor.execute(sql, descripcion)
        db.commit()
    except Exception as e:
        print(e, "***** ERROR al insertar un topic *****")
        db.rollback()


def consultar_posts_por_usuario():
    """  Mostrar la cantidad de posteos por usuario """
    sql = "SELECT id_usuario, count(*) FROM POST GROUP BY id_usuario"
    try:
        cursor.execute(sql)
        datos = cursor.fetchall()
        for i in datos:
            print(i)
    except Exception as e:
        print(e, "***** ERROR al ejecutar la consulta *****")


# insertar_rol("prueba")
# insertar_topic("Prueba")
# insertar_post(1, 3, "Lorem Ipsum-2", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic")
# insertar_comentario(9, 6, "comentario en el post Lorem Ipsum-2")
# insertar_usuario("user_test@test.com", "John", "Test", "123jt", 3)
# consultar_posts_por_usuario()
consultar_todo('TOPIC')

# desconetar del servidor
db.close()
